"""
This is the module that tests the software.
"""

__version__ = '0.1'
__author__ = 'Saul Alonso-Monsalve'
__email__ = "saul.alonso.monsalve@cern.ch"

import pickle
import sys
import os

sys.path.append(os.path.join(sys.path[0], 'modules'))

from classes import Particle, Card, Board 

# load datasets
with open('dataset/particles.p', 'r') as fd:
    particles = pickle.load(fd)
with open('dataset/questions.p', 'r') as fd:
    questions = pickle.load(fd)

board = Board(particles, questions, shuffle=False)
board.setGoal(0)
board.printBoard()
print board.getCardIndexAnswers(0)
print board.checkAnswer(0,1)
print board.checkFaceDown(0, [0,1,2,3,4,5])
board.printBoard()
print board.checkSolution(3)
