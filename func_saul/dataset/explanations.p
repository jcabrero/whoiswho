(lp0
(lp1
S'Yes, all quarks are fermions.'
p2
aS'No, all quarks are fermions.'
p3
aS'No, this is not a force carrier.'
p4
aS'No, a hadron is composed of 2 or more quarks.'
p5
aS'No, a baryon is composed of 3 quarks.'
p6
aS'No, a meson is composed of 2 quarks.'
p7
aS'Yes, it is.'
p8
aS'No, quarks are not leptons.'
p9
aS'No, this is a quark.'
p10
aS'Yes, this has an electric charge of +2/3.'
p11
aS'Yes, this has an electric charge of +2/3.'
p12
aS'No, this has an electric charge of +2/3.'
p13
aS'No, this has an electric charge of +2/3.'
p14
aS'Yes, this has an electric charge of +2/3.'
p15
aS'Yes, quarks have color.'
p16
aS'Yes, this has a mass of 2.3 MeV.'
p17
aS'No, this has a mass of 2.3 MeV.'
p18
aS'Yes, it is.'
p19
aS'No, this is elementary.'
p20
aS'No, this has spin 1/2.'
p21
aS'Yes, this has spin 1/2.'
p22
aS'Yes, it does.'
p23
aS'Yes, it does.'
p24
aS'Yes, it does.'
p25
aS'No, its antiparticle is the antiup.'
p26
aa(lp27
S'Yes, all quarks are fermions.'
p28
aS'No, all quarks are fermions.'
p29
aS'No, this is not a force carrier.'
p30
aS'No, a hadron is composed of 2 or more quarks.'
p31
aS'No, a baryon is composed of 3 quarks.'
p32
aS'No, a meson is composed of 2 quarks.'
p33
aS'Yes, it is.'
p34
aS'No, quarks are not leptons.'
p35
aS'No, this is a quark.'
p36
aS'Yes, this has an electric charge of -1/3.'
p37
aS'No, this has an electric charge of -1/3.'
p38
aS'Yes, this has an electric charge of -1/3.'
p39
aS'No, this has an electric charge of -1/3.'
p40
aS'Yes, this has an electric charge of -1/3.'
p41
aS'Yes, quarks have color.'
p42
aS'Yes, this has a mass of 4.8 MeV.'
p43
aS'No, this has a mass of 4.8 MeV.'
p44
aS'Yes, it is.'
p45
aS'No, this is elementary.'
p46
aS'No, this has spin 1/2.'
p47
aS'Yes, this has spin 1/2.'
p48
aS'Yes, it does.'
p49
aS'Yes, it does.'
p50
aS'Yes, it does.'
p51
aS'No, its antiparticle is the antidown.'
p52
aa(lp53
S'Yes, all quarks are fermions.'
p54
aS'No, all quarks are fermions.'
p55
aS'No, this is not a force carrier.'
p56
aS'No, a hadron is composed of 2 or more quarks.'
p57
aS'No, a baryon is composed of 3 quarks.'
p58
aS'No, a meson is composed of 2 quarks.'
p59
aS'Yes, it is.'
p60
aS'No, quarks are not leptons.'
p61
aS'No, this is a quark.'
p62
aS'Yes, this has an electric charge of +2/3.'
p63
aS'Yes, this has an electric charge of +2/3.'
p64
aS'No, this has an electric charge of +2/3.'
p65
aS'No, this has an electric charge of +2/3.'
p66
aS'Yes, this has an electric charge of +2/3.'
p67
aS'Yes, quarks have color.'
p68
aS'Yes, this has a mass of 1.3 GeV.'
p69
aS'No, this has a mass of 1.3 GeV.'
p70
aS'Yes, it is.'
p71
aS'No, this is elementary.'
p72
aS'No, this has spin 1/2.'
p73
aS'Yes, this has spin 1/2.'
p74
aS'Yes, it does.'
p75
aS'Yes, it does.'
p76
aS'Yes, it does.'
p77
aS'No, its antiparticle is the anticharm.'
p78
aa(lp79
S'Yes, all quarks are fermions.'
p80
aS'No, all quarks are fermions.'
p81
aS'No, this is not a force carrier.'
p82
aS'No, a hadron is composed of 2 or more quarks.'
p83
aS'No, a baryon is composed of 3 quarks.'
p84
aS'No, a meson is composed of 2 quarks.'
p85
aS'Yes, it is.'
p86
aS'No, quarks are not leptons.'
p87
aS'No, this is a quark.'
p88
aS'Yes, this has an electric charge of -1/3.'
p89
aS'No, this has an electric charge of -1/3.'
p90
aS'Yes, this has an electric charge of -1/3.'
p91
aS'No, this has an electric charge of -1/3.'
p92
aS'Yes, this has an electric charge of -1/3.'
p93
aS'Yes, quarks have color.'
p94
aS'Yes, this has a mass of 95 MeV.'
p95
aS'No, this has a mass of 95 MeV.'
p96
aS'Yes, it is.'
p97
aS'No, this is elementary.'
p98
aS'No, this has spin 1/2.'
p99
aS'Yes, this has spin 1/2.'
p100
aS'Yes, it does.'
p101
aS'Yes, it does.'
p102
aS'Yes, it does.'
p103
aS'No, its antiparticle is the antistrange.'
p104
aa(lp105
S'Yes, all quarks are fermions.'
p106
aS'No, all quarks are fermions.'
p107
aS'No, this is not a force carrier.'
p108
aS'No, a hadron is composed of 2 or more quarks.'
p109
aS'No, a baryon is composed of 3 quarks.'
p110
aS'No, a meson is composed of 2 quarks.'
p111
aS'Yes, it is.'
p112
aS'No, quarks are not leptons.'
p113
aS'No, this is a quark.'
p114
aS'Yes, this has an electric charge of +2/3.'
p115
aS'Yes, this has an electric charge of +2/3.'
p116
aS'No, this has an electric charge of +2/3.'
p117
aS'No, this has an electric charge of +2/3.'
p118
aS'Yes, this has an electric charge of +2/3.'
p119
aS'Yes, quarks have color.'
p120
aS'Yes, this has a mass of 173 GeV.'
p121
aS'No, this has a mass of 173 GeV.'
p122
aS'Yes, it is.'
p123
aS'No, this is elementary.'
p124
aS'No, this has spin 1/2.'
p125
aS'Yes, this has spin 1/2.'
p126
aS'Yes, it does.'
p127
aS'Yes, it does.'
p128
aS'Yes, it does.'
p129
aS'No, its antiparticle is the antitop.'
p130
aa(lp131
S'Yes, all quarks are fermions.'
p132
aS'No, all quarks are fermions.'
p133
aS'No, this is not a force carrier.'
p134
aS'No, a hadron is composed of 2 or more quarks.'
p135
aS'No, a baryon is composed of 3 quarks.'
p136
aS'No, a meson is composed of 2 quarks.'
p137
aS'Yes, it is.'
p138
aS'No, quarks are not leptons.'
p139
aS'No, this is a quark.'
p140
aS'Yes, this has an electric charge of -1/3.'
p141
aS'No, this has an electric charge of -1/3.'
p142
aS'Yes, this has an electric charge of -1/3.'
p143
aS'No, this has an electric charge of -1/3.'
p144
aS'Yes, this has an electric charge of -1/3.'
p145
aS'Yes, quarks have color.'
p146
aS'Yes, this has a mass of 4.2 GeV.'
p147
aS'No, this has a mass of 4.2 GeV.'
p148
aS'Yes, it is.'
p149
aS'No, this is elementary.'
p150
aS'No, this has spin 1/2.'
p151
aS'Yes, this has spin 1/2.'
p152
aS'Yes, it does.'
p153
aS'Yes, it does.'
p154
aS'Yes, it does.'
p155
aS'No, its antiparticle is the antibottom.'
p156
aa(lp157
S'Yes, this is a fermion.'
p158
aS'No, this is a fermion.'
p159
aS'No, this is not a force carrier.'
p160
aS'No, a hadron is composed of 2 or more quarks.'
p161
aS'No, a baryon is composed of 3 quarks.'
p162
aS'No, a meson is composed of 2 quarks.'
p163
aS'No. There are only 6 quarks.'
p164
aS'Yes, this is a lepton.'
p165
aS'No, this is a lepton.'
p166
aS'Yes, this has an electric charge of -1.'
p167
aS'No, this has an electric charge of -1.'
p168
aS'Yes, this has an electric charge of -1.'
p169
aS'Yes, this has an electric charge of -1.'
p170
aS'No, this has an electric charge of -1.'
p171
aS'No, this is colorless.'
p172
aS'Yes, this has a mass of 0.5 MeV.'
p173
aS'No, this has a mass of 0.5 MeV.'
p174
aS'Yes, it is.'
p175
aS'No, this is elementary.'
p176
aS'No, this has spin 1/2.'
p177
aS'Yes, this has spin 1/2.'
p178
aS'Yes, it does.'
p179
aS'Yes, it does.'
p180
aS"No, it doesn't."
p181
aS'No, its antiparticle is the positron.'
p182
aa(lp183
S'Yes, this is a fermion.'
p184
aS'No, this is a fermion.'
p185
aS'No, this is not a force carrier.'
p186
aS'No, a hadron is composed of 2 or more quarks.'
p187
aS'No, a baryon is composed of 3 quarks.'
p188
aS'No, a meson is composed of 2 quarks.'
p189
aS'No. There are only 6 quarks (and 6 antiquarks)'
p190
aS'Yes, this is a lepton.'
p191
aS'No, this is a lepton.'
p192
aS'Yes, this has an electric charge of -1.'
p193
aS'No, this has an electric charge of -1.'
p194
aS'Yes, this has an electric charge of -1.'
p195
aS'Yes, this has an electric charge of -1.'
p196
aS'No, this has an electric charge of -1.'
p197
aS'No, this is colorless.'
p198
aS'Yes, this has a mass of 105 MeV.'
p199
aS'No, this has a mass of 105 MeV.'
p200
aS'Yes, it is.'
p201
aS'No, this is elementary.'
p202
aS'No, this has spin 1/2.'
p203
aS'Yes, this has spin 1/2.'
p204
aS'Yes, it does.'
p205
aS'Yes, it does.'
p206
aS"No, it doesn't."
p207
aS'No, its antiparticle is the antimuon.'
p208
aa(lp209
S'Yes, this is a fermion.'
p210
aS'No, this is a fermion.'
p211
aS'No, this is not a force carrier.'
p212
aS'No, a hadron is composed of 2 or more quarks.'
p213
aS'No, a baryon is composed of 3 quarks.'
p214
aS'No, a meson is composed of 2 quarks.'
p215
aS'No. There are only 6 quarks (and 6 antiquarks)'
p216
aS'Yes, this is a lepton.'
p217
aS'No, this is a lepton.'
p218
aS'Yes, this has an electric charge of -1.'
p219
aS'No, this has an electric charge of -1.'
p220
aS'Yes, this has an electric charge of -1.'
p221
aS'Yes, this has an electric charge of -1.'
p222
aS'No, this has an electric charge of -1.'
p223
aS'No, this is colorless.'
p224
aS'Yes, this has a mass of 1.8 MeV.'
p225
aS'No, this has a mass of 1.8 MeV.'
p226
aS'Yes, it is.'
p227
aS'No, this is elementary.'
p228
aS'No, this has spin 1/2.'
p229
aS'Yes, this has spin 1/2.'
p230
aS'Yes, it does.'
p231
aS'Yes, it does.'
p232
aS"No, it doesn't."
p233
aS'No, its antiparticle is the antitau.'
p234
aa(lp235
S'Yes, all neutrinos are fermions.'
p236
aS'No, all neutrinos are fermions.'
p237
aS'No, this is not a force carrier.'
p238
aS'No, a hadron is composed of 2 or more quarks.'
p239
aS'No, a baryon is composed of 3 quarks.'
p240
aS'No, a meson is composed of 2 quarks.'
p241
aS'No. There are only 6 quarks (and 6 antiquarks)'
p242
aS'Yes, all neutrinos are leptons.'
p243
aS'Yes, this is a neutrino.'
p244
aS'No, this is electrically neutral.'
p245
aS'No, this is electrically neutral.'
p246
aS'No, this is electrically neutral.'
p247
aS'Yes, this is electrically neutral.'
p248
aS'No, this is electrically neutral.'
p249
aS'No, this is colorless.'
p250
aS"We don't know... yet! One type of neutrino could be massless."
p251
aS"We don't know... yet! One type of neutrino could be massless."
p252
aS'Yes, it is.'
p253
aS'No, this is elementary.'
p254
aS'No, this has spin 1/2.'
p255
aS'Yes, this has spin 1/2.'
p256
aS"No, it doesn't."
p257
aS'Yes, it does.'
p258
aS"No, it doesn't."
p259
aS"We don't know... yet! This could be its own antiparticle."
p260
aa(lp261
S'Yes, all neutrinos are fermions.'
p262
aS'No, all neutrinos are fermions.'
p263
aS'No, this is not a force carrier.'
p264
aS'No, a hadron is composed of 2 or more quarks.'
p265
aS'No, a baryon is composed of 3 quarks.'
p266
aS'No, a meson is composed of 2 quarks.'
p267
aS'No. There are only 6 quarks (and 6 antiquarks)'
p268
aS'Yes, all neutrinos are leptons.'
p269
aS'Yes, this is a neutrino.'
p270
aS'No, this is electrically neutral.'
p271
aS'No, this is electrically neutral.'
p272
aS'No, this is electrically neutral.'
p273
aS'Yes, this is electrically neutral.'
p274
aS'No, this is electrically neutral.'
p275
aS'No, this is colorless.'
p276
aS"We don't know... yet! One type of neutrino could be massless."
p277
aS"We don't know... yet! One type of neutrino could be massless."
p278
aS'Yes, it is.'
p279
aS'No, this is elementary.'
p280
aS'No, this has spin 1/2.'
p281
aS'Yes, this has spin 1/2.'
p282
aS"No, it doesn't."
p283
aS'Yes, it does.'
p284
aS"No, it doesn't."
p285
aS"We don't know... yet! This could be its own antiparticle."
p286
aa(lp287
S'Yes, all neutrinos are fermions.'
p288
aS'No, all neutrinos are fermions.'
p289
aS'No, this is not a force carrier.'
p290
aS'No, a hadron is composed of 2 or more quarks.'
p291
aS'No, a baryon is composed of 3 quarks.'
p292
aS'No, a meson is composed of 2 quarks.'
p293
aS'No. There are only 6 quarks (and 6 antiquarks)'
p294
aS'Yes, all neutrinos are leptons.'
p295
aS'Yes, this is a neutrino.'
p296
aS'No, this is electrically neutral.'
p297
aS'No, this is electrically neutral.'
p298
aS'No, this is electrically neutral.'
p299
aS'Yes, this is electrically neutral.'
p300
aS'No, this is electrically neutral.'
p301
aS'No, this is colorless.'
p302
aS"We don't know... yet! One type of neutrino could be massless."
p303
aS"We don't know... yet! One type of neutrino could be massless."
p304
aS'Yes, it is.'
p305
aS'No, this is elementary.'
p306
aS'No, this has spin 1/2.'
p307
aS'Yes, this has spin 1/2.'
p308
aS"No, it doesn't."
p309
aS'Yes, it does.'
p310
aS"No, it doesn't."
p311
aS"We don't know... yet! This could be its own antiparticle."
p312
aa(lp313
S'No, this is a boson.'
p314
aS'Yes, this is a boson.'
p315
aS'Yes, this mediates electromagnetic interactions.'
p316
aS'No, a hadron is composed of 2 or more quarks.'
p317
aS'No, a baryon is composed of 3 quarks.'
p318
aS'No, a meson is composed of 2 quarks.'
p319
aS'No. There are only 6 quarks (and 6 antiquarks)'
p320
aS'No, this is not a lepton.'
p321
aS'No, this is a boson.'
p322
aS'No, this is electrically neutral.'
p323
aS'No, this is electrically neutral.'
p324
aS'No, this is electrically neutral.'
p325
aS'Yes, this is electrically neutral.'
p326
aS'No, this is electrically neutral.'
p327
aS'No, this is colorless.'
p328
aS'No, this is massless.'
p329
aS'Yes, it is.'
p330
aS'Yes, it is.'
p331
aS'No, this is elementary.'
p332
aS'Yes, this has spin 1.'
p333
aS'No, this has spin 1.'
p334
aS'Yes, it does. This is the electromagnetic force carrier.'
p335
aS"No, it doesn't."
p336
aS"No, it doesn't."
p337
aS'Yes, this is its own antiparticle.'
p338
aa(lp339
S'No, this is a boson.'
p340
aS'Yes, this is a boson.'
p341
aS'Yes, this mediates weak interactions.'
p342
aS'No, a hadron is composed of 2 or more quarks.'
p343
aS'No, a baryon is composed of 3 quarks.'
p344
aS'No, a meson is composed of 2 quarks.'
p345
aS'No. There are only 6 quarks (and 6 antiquarks)'
p346
aS'No, this is not a lepton.'
p347
aS'No, this is a boson.'
p348
aS'No, this is electrically neutral.'
p349
aS'No, this is electrically neutral.'
p350
aS'No, this is electrically neutral.'
p351
aS'Yes, this is electrically neutral.'
p352
aS'No, this is electrically neutral.'
p353
aS'No, this is colorless.'
p354
aS'Yes, this has a mass of 91 GeV.'
p355
aS'No, this has a mass of 91 GeV.'
p356
aS'Yes, it is.'
p357
aS'No, this is elementary.'
p358
aS'Yes, this has spin 1.'
p359
aS'No, this has spin 1.'
p360
aS"No, it doesn't."
p361
aS'Yes, it does. This is one of the weak force carriers.'
p362
aS"No, it doesn't."
p363
aS'Yes, this is its own antiparticle.'
p364
aa(lp365
S'No, this is a boson.'
p366
aS'Yes, this is a boson.'
p367
aS'Yes, this mediates weak interactions.'
p368
aS'No, a hadron is composed of 2 or more quarks.'
p369
aS'No, a baryon is composed of 3 quarks.'
p370
aS'No, a meson is composed of 2 quarks.'
p371
aS'No. There are only 6 quarks (and 6 antiquarks)'
p372
aS'No, this is not a lepton.'
p373
aS'No, this is a boson.'
p374
aS'Yes, this has an electric charge of +1.'
p375
aS'Yes, this has an electric charge of +1.'
p376
aS'No, this has an electric charge of +1.'
p377
aS'Yes, this has an electric charge of +1.'
p378
aS'No, this has an electric charge of +1.'
p379
aS'No, this is colorless.'
p380
aS'Yes, this has a mass of 80 GeV.'
p381
aS'No, this has a mass of 80 GeV.'
p382
aS'Yes, it is.'
p383
aS'No, this is elementary.'
p384
aS'Yes, this has spin 1.'
p385
aS'No, this has spin 1.'
p386
aS'Yes, it does.'
p387
aS'Yes, it does. This is one of the weak force carriers.'
p388
aS"No, it doesn't."
p389
aS'No, its antiparticle is the W-.'
p390
aa(lp391
S'No, this is a boson.'
p392
aS'Yes, this is a boson.'
p393
aS'Yes, this mediates weak interactions.'
p394
aS'No, a hadron is composed of 2 or more quarks.'
p395
aS'No, a baryon is composed of 3 quarks.'
p396
aS'No, a meson is composed of 2 quarks.'
p397
aS'No. There are only 6 quarks (and 6 antiquarks)'
p398
aS'No, this is not a lepton.'
p399
aS'No, this is a boson.'
p400
aS'Yes, this has an electric charge of -1.'
p401
aS'No, this has an electric charge of -1.'
p402
aS'Yes, this has an electric charge of -1.'
p403
aS'Yes, this has an electric charge of -1.'
p404
aS'No, this has an electric charge of -1.'
p405
aS'No, this is colorless.'
p406
aS'Yes, this has a mass of 80 GeV.'
p407
aS'No, this has a mass of 80 GeV.'
p408
aS'Yes, it is.'
p409
aS'No, this is elementary.'
p410
aS'Yes, this has spin 1.'
p411
aS'No, this has spin 1.'
p412
aS'Yes, it does.'
p413
aS'Yes, it does. This is one of the weak force carriers.'
p414
aS"No, it doesn't."
p415
aS'No, its antiparticle is the W+.'
p416
aa(lp417
S'No, this is a boson.'
p418
aS'Yes, this is a boson.'
p419
aS'Yes, this mediates strong interactions.'
p420
aS'No, a hadron is composed of 2 or more quarks.'
p421
aS'No, a baryon is composed of 3 quarks.'
p422
aS'No, a meson is composed of 2 quarks.'
p423
aS'No. There are only 6 quarks (and 6 antiquarks)'
p424
aS'No, this is not a lepton.'
p425
aS'No, this is a boson.'
p426
aS'No, this is electrically neutral.'
p427
aS'No, this is electrically neutral.'
p428
aS'No, this is electrically neutral.'
p429
aS'Yes, this is electrically neutral.'
p430
aS'No, this is electrically neutral.'
p431
aS'Yes, gluons have color.'
p432
aS'No, this is massless.'
p433
aS'Yes, it is.'
p434
aS'Yes, it is.'
p435
aS'No, this is elementary.'
p436
aS'Yes, this has spin 1.'
p437
aS'No, this has spin 1.'
p438
aS"No, it doesn't."
p439
aS"No, it doesn't."
p440
aS'Yes, it does. This is the strong force carrier.'
p441
aS'Yes, the antiparticle of a gluon is another gluon.'
p442
aa(lp443
S'No, this is a boson.'
p444
aS'Yes, this is a boson.'
p445
aS'No, this is not a force carrier.'
p446
aS'No, a hadron is composed of 2 or more quarks.'
p447
aS'No, a baryon is composed of 3 quarks.'
p448
aS'No, a meson is composed of 2 quarks.'
p449
aS'No. There are only 6 quarks (and 6 antiquarks)'
p450
aS'No, this is not a lepton.'
p451
aS'No, this is a boson.'
p452
aS'No, this is electrically neutral.'
p453
aS'No, this is electrically neutral.'
p454
aS'No, this is electrically neutral.'
p455
aS'Yes, this is electrically neutral.'
p456
aS'No, this is electrically neutral.'
p457
aS'No, this is colorless.'
p458
aS'Yes, this has a mass of 125 GeV.'
p459
aS'No, this has a mass of 125 GeV.'
p460
aS"We don't know... yet! The Higgs could be composite."
p461
aS"We don't know... yet! The Higgs could be composite."
p462
aS'Yes, this has spin 0.'
p463
aS'No, this has spin 0.'
p464
aS"No, it doesn't."
p465
aS'Yes, it does.'
p466
aS"No, it doesn't."
p467
aS'Yes, this is its own antiparticle.'
p468
aa(lp469
S'Yes, this is a fermion.'
p470
aS'No, this is a fermion.'
p471
aS'No, this is not a force carrier.'
p472
aS'Yes, this is composed of more than one quark.'
p473
aS'Yes, this is composed of 3 quarks.'
p474
aS'No, a meson is composed of 2 quarks.'
p475
aS'No. There are only 6 quarks (and 6 antiquarks)'
p476
aS'No, this is not a lepton.'
p477
aS'No, this is a hadron.'
p478
aS'Yes, this has an electric charge of +1.'
p479
aS'Yes, this has an electric charge of +1.'
p480
aS'No, this has an electric charge of +1.'
p481
aS'Yes, this has an electric charge of +1.'
p482
aS'No, this has an electric charge of +1.'
p483
aS'No, this is colorless.'
p484
aS'Yes, this has a mass of 940 MeV.'
p485
aS'No, this has a mass of 940 MeV.'
p486
aS'No, this is composed of 3 quarks.'
p487
aS'Yes, this is composed of 3 quarks.'
p488
aS'No, this has spin 1/2.'
p489
aS'Yes, this has spin 1/2.'
p490
aS'Yes, it does.'
p491
aS'Yes, it does.'
p492
aS'Yes, it does.'
p493
aS'No, its antiparticle is the antiproton.'
p494
aa(lp495
S'Yes, this is a fermion.'
p496
aS'No, this is a fermion.'
p497
aS'No, this is not a force carrier.'
p498
aS'Yes, this is composed of more than one quark.'
p499
aS'Yes, this is composed of 3 quarks.'
p500
aS'No, a meson is composed of 2 quarks.'
p501
aS'No. There are only 6 quarks (and 6 antiquarks)'
p502
aS'No, this is not a lepton.'
p503
aS'No, this is a hadron.'
p504
aS'No, this is electrically neutral.'
p505
aS'No, this is electrically neutral.'
p506
aS'No, this is electrically neutral.'
p507
aS'Yes, this is electrically neutral.'
p508
aS'No, this is electrically neutral.'
p509
aS'No, this is colorless.'
p510
aS'Yes, this has a mass of 940 MeV.'
p511
aS'No, this has a mass of 940 MeV.'
p512
aS'No, this is composed of 3 quarks.'
p513
aS'Yes, this is composed of 3 quarks.'
p514
aS'No, this has spin 1/2.'
p515
aS'Yes, this has spin 1/2.'
p516
aS'Yes, it does.'
p517
aS'Yes, it does.'
p518
aS'Yes, it does.'
p519
aS'No, its antiparticle is the antineutron.'
p520
aa.