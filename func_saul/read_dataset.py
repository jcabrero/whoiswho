"""
This is the module that reads the dataset and generates the serialized files.
"""

__version__ = '0.1'
__author__ = 'Saul Alonso-Monsalve'
__email__ = "saul.alonso.monsalve@cern.ch"

import pickle
import csv
import sys
import os

sys.path.append(os.path.join(sys.path[0], 'modules'))

from classes import Particle

with open('dataset/dataset.csv', 'r') as fd:
    with open('dataset/explanations.csv', 'r') as fd2:
        lines = csv.reader(fd, delimiter=',')
        questions = lines.next()[1:]
        particles = []
        for line in lines:
            particles.append(Particle(name=line[0], answers=[int(x) for x in line[1:]]))

        with open('dataset/questions.p', 'w') as of1:
            pickle.dump(questions, of1)
        with open('dataset/particles.p', 'w') as of2:
            pickle.dump(particles, of2)

        lines2 = csv.reader(fd2, delimiter=',')
        lines2.next()
        explanations = []
        for line in lines2:
            explanations.append([x for x in line[1:]])

        with open('dataset/explanations.p', 'w') as of3:
            pickle.dump(explanations, of3)
