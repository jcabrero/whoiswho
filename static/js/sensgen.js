function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function get_array_current_values(){
  var array = [
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11'],
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11'],
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11'],
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11'],
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11'],
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11'],
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11'],
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11'],
    ['input1','input2','input3','input4','input5','input6', 'input7','input8','input9','input10','input11']
  ];
  for( var j = 1; j <= 8; j++){
      for(var i = 0; i < 11; ++i){
      var key = "sensor"+j+"-"+i;
      if(document.getElementById(key) != null){
        array[j][i] = document.getElementById(key).value;
      }
      else{
        array[j][i] = "x";
      }
    }
  }

  print(array);

}
