
var chart; // global
var chart1;
var chart2;
var prev = new Date().getTime();
function requestData() {
    $.ajax({
        url: 'random',
        success: function(results) {
            var series = chart.series[0],
                shift = series.data.length > 20; // shift if the series is 
                               // longer than 20
            shift = false;
            console.log(results);
            point = JSON.parse(results);;

            var now = new Date().getTime();
            timepoint = now - prev;
            prev = now;
            
            console.log(point.x)
            console.log(point.y)
            console.log(timepoint)
          
            var char_point_a = {x: point.x, y: point.y};
            var char_point_b = {x: point.x, y: timepoint};
            var char_point_c = {x: point.x, y: point.time};
            // add the point
            chart.series[0].addPoint(char_point_a, true, shift);
            chart1.series[0].addPoint(char_point_b, true, shift);
            chart2.series[0].addPoint(char_point_c, true, shift);
            // call it again after one second
            setTimeout(requestData, 1000);    
        },
        cache: false
    });
}


document.addEventListener('DOMContentLoaded', function() {
    chart = Highcharts.chart('graph1', {
        chart: {
            type: 'spline',
            events: {
                load: requestData
            }
        },
        title: {
            text: 'Live random data'
        },
        xAxis: {
            tickInterval: 1,
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Value',
                margin: 80
            }
        },
        series: [{
            name: 'Time Difference',
            data: []
        }]
    });

    chart1 = Highcharts.chart('graph2', {
        chart: {
            type: 'spline',
            events: {
                load: requestData
            }
        },
        title: {
            text: 'Delay in time'
        },
        xAxis: {
            title:{
                text: 'Milliseconds'
            },
            tickInterval: 1,
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Value',
                margin: 80
            }
        },
        series: [{
            name: 'Random data',
            data: []
        }]
    });

    chart2 = Highcharts.chart('graph3', {
        chart: {
            type: 'spline',
            events: {
                load: requestData
            }
        },
        title: {
            text: 'Delay in time'
        },
        xAxis: {
            title:{
                text: 'Milliseconds'
            },
            tickInterval: 1,
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Value',
                margin: 80
            }
        },
        series: [{
            name: 'Random data',
            data: []
        }]
    });        
});



/*$(function () { 
    var myChart = Highcharts.chart('graph2', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Fruit Consumption'
        },
        xAxis: {
            categories: ['Apples', 'Bananas', 'Oranges']
        },
        yAxis: {
            title: {
                text: 'Fruit eaten'
            }
        },
        series: [{
            name: 'Jane',
            data: [1, 0, 4]
        }, {
            name: 'John',
            data: [5, 7, 3]
        }]
    });
});*/
