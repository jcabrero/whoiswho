var number_of_sensors = 0;
sensors = null;
var table_row;
var max_sensors = 8;

$('.dropdown-trigger').dropdown();
document.addEventListener('DOMContentLoaded', function() {
	console.log("second")
	var elems = document.querySelectorAll('select');
	var options = document.querySelectorAll('option');
	var instances = M.FormSelect.init(elems, options);
	$("#add_button").on("click", function(){
		if (number_of_sensors == max_sensors - 1){
			$("#add_button").remove();
			console.log("removed");
		} 
		console.log("CLICK!");
		generate_row(sensors);
		var elems = document.querySelectorAll('select');
		var options = document.querySelectorAll('option');
		var instances = M.FormSelect.init(elems, options);
	});
});


$( document ).ready(function() {
	console.log("first");
	get_remote();
	generate_row(sensors);
});

//Generates a row of content based on the results passed as parameter
function generate_row(results){
 	if (number_of_sensors == max_sensors) return null;
 	var index = ++ number_of_sensors;
 	var color = ((index % 2) == 0)? "grey lighten-3" : "grey lighten-2";
	var txt = "<div class=\"col s10 push-s1 "+ color +"\">\n\t<div class=\"col s12 m1 theader center-align\">\n\t\t<p class=\"\"> Sensor"+ index +"</p>\n\t</div>\n";

	for (i = 0; i < 11; ++i){
		txt +="\t<div class=\"col s12 m1 center-align\">\n\t\t\t<select name=\"sensor" + index +"-"+ i + "\" id=\"sensor" + index + "-" + i +"\">\n";
		for (j = 0; j < results.length; j++){
    		//console.log(results[j]);
 			txt += "\t\t\t\t<option value=\""+results[j].sensor_name+"\"> "+ results[j].sensor_name + " </option>\n"
    	}
    	txt += "\t\t\t</select>\n\t\t</div>\n" 
	}
	 txt += "</div>\n";
	 name = "#table-body";
	 console.log("Created a new row");
	 $(name).append(txt);
	 return txt;
}

//Gets the data of the sensors from the database, and creates the HTML equivalent code dynamically.
function get_remote(){
	var i = $.ajax({
		type: "POST",
    	url: 'http://yunirradrh.cern.ch:8080/sensors',
    	async: false,
    	dataType: 'json',
    	success: function(results) {
  		
    		//$('#table-body tr:last').after(result);
    		//$('#myTable > div:last-child').append(result);
    		sensors = results;
    		console.log("Asynchronous call get");
    	}
	})
	return i;
}